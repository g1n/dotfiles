require "paq" {
  "savq/paq-nvim";
  "savq/melange";
  "kyazdani42/nvim-web-devicons";
  "kyazdani42/nvim-tree.lua";
}

-- Nvim Tree

require'nvim-tree'.setup {}

-- Keybindings
local map = vim.api.nvim_set_keymap

map('', '<F2>', ':bNext\n', {})
map('', '<F3>', ':bprevious\n', {})
map('', '<F4>', ':source ~/.config/nvim/init.lua\n', {})

map('', '<C-n>', ':NvimTreeToggle\n', {})
map('', '<leader>r', ':NvimTreeRefresh\n', {})
map('', '<leader>n', ':NvimTreeFindFile\n', {})

-- Settings
local set = vim.opt
local cmd = vim.cmd

--- Theme
cmd 'colorscheme melange'
set.termguicolors = true

-- Misc

set.nu = true

--- Tab size

set.tabstop = 2
set.shiftwidth = 2
set.softtabstop = 2
set.expandtab = true

